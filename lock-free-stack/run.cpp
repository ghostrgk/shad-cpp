#include <benchmark/benchmark.h>

#include <stack.h>

#include <iostream>

#include <hazard_ptr.h>

std::unique_ptr<Stack<int>> stack;

void StressPush(benchmark::State& state) {
    RegisterThread();

    if (state.thread_index == 0) {
        stack = std::make_unique<Stack<int>>();
    }

    while (state.KeepRunning()) {
        stack->Push(0);
    }

    if (state.thread_index == 0) {
        stack.reset();
    }

    UnregisterThread();
}

void StressPushPop(benchmark::State& state) {
    RegisterThread();

    if (state.thread_index == 0) {
        stack = std::make_unique<Stack<int>>();
    }

    if (state.thread_index < state.range(0)) {
        while (state.KeepRunning()) {
            stack->Push(0);
        }
    } else {
        int value;
        while (state.KeepRunning()) {
            (void)stack->Pop(&value);
        }
    }

    if (state.thread_index == 0) {
        stack.reset();
    }

    UnregisterThread();
}

BENCHMARK(StressPush)->Threads(8);
BENCHMARK(StressPushPop)->Threads(50)->Arg(42)->UseRealTime();
BENCHMARK(StressPushPop)->Threads(80)->Arg(75)->UseRealTime();
BENCHMARK(StressPushPop)->Threads(20)->Arg(15)->UseRealTime();
BENCHMARK(StressPushPop)->Threads(60)->Arg(50)->UseRealTime();

BENCHMARK_MAIN();
